<?php
require_once 'dbh.inc.php';
require_once 'function.inc.php';

if (isset($_POST["submit3"])) {

    $purPicName = $_POST["picname"];
    $purName = $_POST["title"];
    $purTitle = $_POST["name"];
    $purZipcode = $_POST["zip"];
    $purCity = $_POST["city"];
    $purStreet = $_POST["street"];
    $purHouse = $_POST["house"];
    if (emptyInputSignup($purPicName, $purName, $purTitle, $purZipcode, $purCity, $purStreet, $purHouse) !== false) {
        header("location: main.php?error=emptyinput");
        exit();
    }
    createPurchase($conn3, $purPicName, $purName, $purTitle, $purZipcode, $purCity, $purStreet, $purHouse);
} else {
    header("location: main.php");
    exit();
}
