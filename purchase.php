<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="icon" href="kis.jpg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/purchase.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous" />
  <title>Document</title>
  <script src="javas/main.js" defer></script>
</head>

<body>
  <div class="container-fluid">
    <div class="row ">
      <div class="col-md-12 col-sm-12 col-xl-12">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <nav class="nav-main">
                <div class="btn-toggle-nav" onclick="toggleNav()"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></div>
                <ul>
                  <li><a href="index.php">Home</a></li>
                  <?php
                  if (isset($_SESSION["useruid"])) {
                    echo "<li><a href='purchase.php'>Purchase</a></li>";
                    echo "<li><a href='logout.php'>Log out</a></li>";
                  } else {
                    echo "<li><a href='login.php'>Log in</a></li>";
                    echo "<li><a href='reg.php'>Registration</a></li>";
                  }

                  ?>
                </ul>
            </div>
            </nav>

          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <aside class="nav-sidebar">
                <ul>
                  <li><span>
                      <?php
                      if (isset($_SESSION["useruid"])) {

                        echo "<p>Hello there " . $_SESSION["useruid"] . " !" . "</p>";
                      } else {
                        echo "<p>Jelenzkezz be!</p>";
                      }
                      ?>
                    </span></li>
                  <li><a href="purchase.php">Purchase</a></li>
                  <li><a href="gallery.php">Shop</a></li>
                  <li><a href="videos.php">Videos</a></li>
                  <li><a href="option.php">Settings</a></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
  include_once 'dbh.inc.php';

  if (isset($_GET['imageSrc'])) {
    $imageSrc = $_GET['imageSrc'];
    $title = $_GET['title'];

    echo '<div class="container">
            <div class="row">
              <div class="col-md-12">
                <h2 class="text-center">' . $title . '</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8 offset-md-2">
                <img src="' . $imageSrc . '" class="img-fluid mx-auto d-block" alt="">
              </div>
            </div>
          </div>';
  } else {
    header("Location: index.php");
    exit();
  }
  ?>
  <form action="purchase.inc.php" method="post">
    <div class="container center ">
      <div class="col ">
        <section class="payment p-5 pt-4 pb-0">
          <div class="row ">
            <div class="col p-2 col-md1">
              <div class="text">
                <input type="hidden" name="picname" value="<?php echo $imageSrc; ?>">
                <input type="hidden" name="title" value="<?php echo $title; ?>">
                <span name="title"><?php echo $title; ?></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col p-2 col-md1">
              <div class="text">
                <span>Name:</span>
                <input type="text" pattern="[A-Za-z\s]+" maxlength="16" class="card-number-input" name="name" placeholder="Mark Clifford ">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col p-2 col-md1">
              <div class="text">
                <span>Age for discount:</span>
                <input type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="card-holder-input" name="zip" placeholder="61">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col p-2 col-md1">
              <div class="text">
                <div class="text">
                  <span>City:</span>
                  <input type="text" pattern="[A-Za-z\s]+" class="card-holder-input" name="city" placeholder="Los Angels">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col p-2 col-md1">
              <div class="text">
                <span>Street Name:</span>
                <input type="text" pattern="[A-Za-z\s]+" class="card-holder-input" name="street" placeholder="Vines Creek Rd.">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col p-2 col-md1">
              <div class="text">
                <span>House Number:</span>
                <input type="text" maxlength="10" class="cvv-input" name="house" placeholder="64/b">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col p-2 col-md1">
              <button type="submit" name="submit3">Purchase</button>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
  <footer class="p-5 pb-0">
    <div class="container mt-5 ">
      <div class="row p-3">
        <div class="col-md-3 col-sm-6">
          <div class="aboutus">
            <h2>About us</h2>
            <p class="p-1">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.
            </p>

            <ul class="sci">
              <li class="you">
                <a href="https://www.youtube.com/results?search_query=starset" target="_blank"><i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i></a>
              </li>
              <li class="face">
                <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i></a>
              </li>
              <li class="twitch">
                <a href="https://www.twitch.tv/" target="_blank"><i class="fa fa-twitch fa-3x" aria-hidden="true"></i></a>
              </li>
              <li class="insta">
                <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram fa-3x" aria-hidden="true"></i></a>
              </li>
            </ul>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="quickLinks">
            <h2>Quick Links</h2>
            <ul class="qlinfo">
              <li><a href="reg.html">FAQ</a></li>
              <li><a href="#">Terms and Consitions</a></li>
              <li><a href="#">Privacy Polcicy</a></li>
              <li><a href="#">Help</a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="sec contact">
            <h2>Conctact</h2>
            <ul class="info p-1">
              <li>
                <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <p>
                  <a href="mailto:tdominik86@gmail.com">tdominik86@gmail.com</a>
                </p>
              </li>
              <li>
                <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                <p><a href="tel:36202663811">+36232323321</a><br /></p>
              </li>
              <li>
                <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                <span>123123</br> asd </br> Masada4.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="asd col-md-3 col-sm-6">
          <div class="maptest map">
            <h2>Map</h2>
            <ul class="qlmap p-1">
              <iframe src="https://www.google.com/maps/embed" width="100%" height="200" style="border: 0" allowfullscreen="" loading="lazy"></iframe>
            </ul>
          </div>
        </div>
      </div>

      <div class="copy col-md-12 ">
        <p class="asd text-center">Copyright © created by Dominik Tarjáni</p>
      </div>
    </div>
    </div>
  </footer>
</body>


</html>