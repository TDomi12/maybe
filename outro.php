<?php 
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
      href="https://fonts.googleapis.com/css?family=Cookie"
      rel="stylesheet"
    />
    <link rel="stylesheet" type="text/css" href="css/intro.css" />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0="
      crossorigin="anonymous"
    />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0"
      crossorigin="anonymous"
    />
    <title>Outro</title>
</head>
<body>
  <div class="menu container-fluid ">
    <div class="row align-items-center">
      <div class="col-md-12 col-sm-12 ">
        <div class="asd text-center">
          <span>G</span>
          <span>O</span>
          <span>O</span>
          <span>D</span>
          <span>B</span>
          <span>Y</span>
          <span>E</span>
          <span>!</span>
          <div class="asdasd pt-5">
            <form method="post" action="intro.php">
              <button name="kilep">Back</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid d-flex justify-content-center fixed-bottom pl-0 pr-0">
    <ul class="sci">
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <li class="you">
            <a 
            href="https://www.youtube.com/results?search_query=starset"
            target="_blank"
            ><i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i
            ></a>
          </li>
        </div>

        <div class="col-md-3 col-sm-3">
          <li class="face">
            <a  href="https://www.facebook.com/" target="_blank"
            ><i
            class="fa fa-facebook-official fa-3x"
            aria-hidden="true"
            ></i
            ></a>
          </li>
        </div>

        <div class="col-md-3 col-sm-3">
          <li class="twitch">
            <a href="https://www.twitch.tv/" target="_blank"
            ><i class="fa fa-twitch fa-3x" aria-hidden="true"></i
            ></a>
          </li>
        </div>

        <div class="col-md-3 col-sm-3">
          <li class="insta">
            <a href="https://www.instagram.com/" target="_blank"
            ><i class="fa fa-instagram fa-3x" aria-hidden="true"></i
            ></a>
          </li>
        </div>
      </div>
    </ul>
  </div>
</body>
</html>