<?php
include_once "dbh.inc.php";

if (isset($_POST['submit2'])) {
    $newFileName = $_POST['filename'];
    if (empty($newFileName)) {
        $newFileName = "gallery";
    } else {
        $newFileName = strtolower(str_replace(" ", "-", $newFileName));
    }
    $imgTitle = $_POST['filetitle'];
    $imgDesc = $_POST['filedesc'];

    $file = $_FILES['file'];


    $fileName = $file["name"];
    $fileType = $file["type"];
    $fileTempName = $file["tmp_name"];
    $fileError = $file["error"];
    $fileSize = $file["size"];

    $fileExt = explode(".", $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array("jpg", "jpeg", "png");

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 20000000) {
                $imageFullName = $newFileName . "."  . uniqid("", true) . "." . $fileActualExt;
                $fileDestination = "kepek/" . $imageFullName;

                if (empty($imgTitle) || empty($imgDesc)) {
                    header("location: gallery.php?upload=emty");
                    exit;
                } else {
                    $sql2 = "SELECT * FROM gallery;";
                    $stmt2 = mysqli_stmt_init($conn2);
                    if (!mysqli_stmt_prepare($stmt2, $sql2)) {
                        header("location: gallery.php?error=stmtfailed");
                        exit();
                    } else {
                        mysqli_stmt_execute($stmt2);
                        $result = mysqli_stmt_get_result($stmt2);
                        $rowCount = mysqli_num_rows($result);
                        $setImageOrder = $rowCount + 1;
                        $sql2 = "INSERT INTO gallery (tilteGallery, descGallery, imgFullNameGallery, orderGallery) VALUES (?, ?, ?, ?);";

                        if (!mysqli_stmt_prepare($stmt2, $sql2)) {
                            header("location: gallery.php?error=stmtfailed");
                            exit();
                        } else {
                            mysqli_stmt_bind_param($stmt2, "ssss", $imgTitle, $imgDesc, $imageFullName, $setImageOrder);
                            mysqli_stmt_execute($stmt2);


                            move_uploaded_file($fileTempName, $fileDestination);
                            header("location: gallery.php?upload=success");
                        }
                    }
                }
            } else {
                echo "File Size is too big";
                exit;
            }
        } else {
            echo "You had an error";
            exit;
        }
    } else {
        echo "You need to upload a proper file type";
        exit;
    }
}
