<?php
session_start();
/*if(!isset($_SESSION['authenticate']))
{
    header("location: intro.php");
    exit;
}
else {
 }
 session_destroy();*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Main Page</title>
  <link rel="icon" href="kis.jpg">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous" />
  <script src="javas/main.js" defer></script>
</head>

<body>
  <div class="container-fluid">
    <div class="row ">
      <div class="col-md-12 col-sm-12 col-xl-12">

        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <nav class="nav-main">
                <div class="btn-toggle-nav" onclick="toggleNav()"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></div>
                <ul>
                  <li><a href="index.php">Home</a></li>
                  <?php
                  if (isset($_SESSION["useruid"])) {
                  } else {
                    echo "<li><a href='login.php'>Log in</a></li>";
                    echo "<li><a href='reg.php'>Registration</a></li>";
                  }
                  ?>
                  <div class="lightm">
                    <input type="checkbox" id="color" name="" />
                  </div>
                </ul>
            </div>
            </nav>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12">
              <aside class="nav-sidebar">
                <ul>
                  <li><span>
                      <?php
                      if (isset($_SESSION["useruid"])) {

                        echo "<p>Hello there " . $_SESSION["useruid"] . " !" . "</p>";
                      } else {
                        echo "<p >Login Please!</p>";
                      }
                      ?>
                    </span></li>
                  <li><a href="purchase.php">Purchase</a></li>
                  <li><a href="gallery.php">Shop</a></li>
                  <li><a href="">Videos</a></li>
                  <li><a href="">Settings</a></li>
                  <li><a href="logout.php">Logout</a></li>
                </ul>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <header>
    <form id="form">
      <input type="text" class="search" id="search" placeholder="Search..." />
    </form>
  </header>
  <main id="main"></main>

  <footer class="p-5 pt-4 pb-0">
    <div class="container mt-5 ">
      <div class="row p-3">
        <div class="col-md-12 col-sm-12 col-12 pb-2 text-center">
          <h1>information</h1>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="aboutus">
            <h2>About us</h2>
            <p class="p-1">
              Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.
            <ul class="sci1">
              <li class="you">
                <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i></a>
              </li>
              <li class="face">
                <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i></a>
              </li>
              <li class="twitch">
                <a href="https://www.twitch.tv/" target="_blank"><i class="fa fa-twitch fa-3x" aria-hidden="true"></i></a>
              </li>
              <li class="insta">
                <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram fa-3x" aria-hidden="true"></i></a>
              </li>
            </ul>
            </p>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="quickLinks">
            <h2>Quick Links</h2>
            <ul class="qlinfo">
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Terms and Consitions</a></li>
              <li><a href="#">Privacy Polcicy</a></li>
              <li><a href="#">Help</a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-3 col-sm-6">
          <div class="sec contact">
            <h2>Conctact</h2>
            <ul class="info p-1">
              <li>
                <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <p class="text-break">
                  <a href="mailto:tdominik86@gmail.com">tdominik86@gmail.com</a>
                </p>
              </li>
              <li>
                <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                <p><a href="tel:36202663811">+36232323321</a><br /></p>
              </li>
              <li>
                <span><i class="fa fa-map-marker fa" aria-hidden="true"></i></span>
                <span>123123</br>asd</br> Masada4.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="asd col-md-3 col-sm-6">
          <div class="maptest map">
            <h2>Map</h2>
            <ul class="qlmap p-1">
              <iframe src="https://www.google.com/maps/embed" width="100%" height="180" style="border: 0" allowfullscreen="" loading="lazy"></iframe>
            </ul>
          </div>
        </div>
        <div class="col-md-12 pt-0 mg-0">
          <ul class="sci">
            <li class="you">
              <a href="https://www.youtube.com/results?search_query=starset" target="_blank"><i class="fa fa-youtube-play fa-3x" aria-hidden="true"></i></a>
            </li>
            <li class="face">
              <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i></a>
            </li>
            <li class="twitch">
              <a href="https://www.twitch.tv/" target="_blank"><i class="fa fa-twitch fa-3x" aria-hidden="true"></i></a>
            </li>
            <li class="insta">
              <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram fa-3x" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
        <div class="copy col-md-12 pt-0 mg-0">
          <p class="asd text-center">Copyright © created by Dominik Tarjáni</p>
        </div>
      </div>
    </div>
    </div>
  </footer>
</body>

</html>