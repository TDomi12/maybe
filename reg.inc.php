<?php
require_once 'dbh.inc.php';
require_once 'function.inc.php';

if (isset($_POST["submit"])) {

    $name =  $_POST["name"];
    $username =  $_POST["uid"];
    $email =  $_POST["email"];
    $pwd =  $_POST["pwd1"];
    $pwdRepeat =  $_POST["pwd2"];



    if (emptyInputSignup($name, $username, $email, $pwd, $pwdRepeat) !== false) {
        header("location: reg.php?error=emptyinput");
        exit();
    }


    if (invalidEmail($email) !== false) {
        header("location: reg.php?error=invalidEmail");
        exit();
    }
    if (pwdMatch($pwd, $pwdRepeat) !== false) {
        header("location: reg.php?error=passworddontmatch");
        exit();
    }
    if (uidExists($conn, $username, $email) !== false) {
        header("location: reg.php?error=uidExists");
        exit();
    }
    createUser($conn, $name, $email, $username, $pwd);
} else {

    header("location: reg.php");
    exit();
}
