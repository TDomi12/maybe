<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Registration</title>
</head>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/regstyle.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous" />

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <nav class="nav-main">
          <div class="btn-toggle-nav" onclick="toggleNav()">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
          </div>
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="gallery.php">Shop</a></li>
            <li><a href="login.php">Login</a></li>
            <li><input class="lightm" type="checkbox" id="color" name="" />></li>
          </ul>
        </nav>
      </div>
    </div>
    <aside class="nav-sidebar">
      <ul>
        <li><span>
            <?php
            if (isset($_SESSION["useruid"])) {

              echo "<p>Hello there " . $_SESSION["useruid"] . " !" . "</p>";
            } else {
              echo "<p>Jelenzkezz be!</p>";
            }
            ?>
          </span></li>
        <li><a href="purchase.php">purchase</a></li>
        <li><a href="gallery.php">Gallery</a></li>
        <li><a href="videos.php">Videos</a></li>
        <li><a href="option.php">Settings</a></li>
        <li><a href="logout.php">Logout</a></li>
      </ul>
    </aside>
  </div>

  <div class="regist container center">
    <div class="col">
      <div class="row">
        <div class="Sign p-2 col-md1">
          <span>Registration Form </span>
        </div>
        <form action="reg.inc.php" method="post">
          <div class="Tnev p-2 col-md1">
            <label>Full Name</label>
            <input type="text" class="form-control" id="Veznev" name="name" placeholder="Pl.:Jenő" />
          </div>
          <div class="Uname p-2 col-md1">
            <label>NickName</label>
            <input type="text" class="form-control" id="Kernev" name="uid" placeholder="Pl.: Kiss" />
          </div>
          <div class="email p-2 col-md1">
            <label>Email</label>
            <input type="email" class="form-control" id="Emailcim" aria-describedby="emailHelp" name="email" placeholder="Enter email" />
          </div>
          <div class="jelszo p-2 col-md1">
            <label>Password</label>
            <input type="password" class="form-control" id="Pass1" name="pwd1" placeholder="Password" />
          </div>
          <div class="jelszouj p-2 pb-3 col-md1">
            <label>Password again</label>
            <input type="password" class="form-control" id="Pass2" name="pwd2" placeholder="Password" />
          </div>
          <div class="Summit d-grid gap-2 col-md1 mb-2">
            <button class="btn btn-primary" name="submit" type="submit">Registration</button>
          </div>
          <?php
          if (isset($_GET["error"])) {
            if ($_GET["error"] == "emptyinput")
              echo "<p>Fill all fields</p>";

            else if ($_GET["error"] == "invalidUid") {
              echo "<p>Invalid name</p>";
            } else if ($_GET["error"] == "invalidEmail") {
              echo "<p>Invalid email address</p>";
            } else if ($_GET["error"] == "passworddontmatch") {
              echo "<p>Password does not match</p>";
            } else if ($_GET["error"] == "uidExists") {
              echo "<p>hasznalatban van a nev</p>";
            } else if ($_GET["error"] == "stmtfailed") {
              echo "<p>Somethign went wrong</p>";
            } else if ($_GET["error"] == "none") {
              echo "<p>sikerült regisztrálni</p>";
              // header("location: login.php");
              //exit;
            }
          }
          ?>
        </form>
      </div>
    </div>
  </div>
</body>
<script src="javas/reg.js"></script>

</html>