<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Log in</title>
</head>

<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/regstyle.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous" />

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <nav class="nav-main">
          <div class="btn-toggle-nav" onclick="toggleNav()">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
          </div>
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="gallery.php">Gallery</a></li>
            <li><a href="reg.php">Registration</a></li>
            <li><input class="lightm" type="checkbox" id="color" name="" /></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>

  <aside class="nav-sidebar">
    <ul>
      <li><span>
          <?php
          if (isset($_SESSION["useruid"])) {
            echo "<p>Hello there " . $_SESSION["useruid"] . " !" . "</p>";
          } else {
            echo "<p>Jelenzkezz be!</p>";
          }
          ?>
        </span></li>
      <li><a href="purchase.php">Purchase</a></li>
      <li><a href="gallery.php">Shop</a></li>
      <li><a href="videos.php">Videos</a></li>
      <li><a href="">Settings</a></li>
      <li><a href="logout.php">Logout</a></li>
    </ul>
  </aside>

  <div class=" container regist">
    <div class="row">
      <div class="Sign p-2 col-md1">
        <span>Login Form </span>
      </div>
      <form action="login.inc.php" method="post">
        <div class="veteteknev p-2 col-md1">
          <label>Email/Name</label>
          <input type="text" class="form-control" id="Veznev" name="uid" placeholder="Pl.:Mark" />
        </div>

        <div class="jelszo p-2 col-md1">
          <label>Password</label>
          <input type="password" class="form-control" id="Pass1" name="pwd1" placeholder="Password" />
        </div>

        <div class="Summit d-grid gap-2 col-md1 mb-2">
          <button class="btn btn-primary" type="submit" name="submit">Log in</button>
        </div>
      </form>
      <?php
      if (isset($_GET["error"])) {
        if ($_GET["error"] == "emptyinput")
          echo "<p>Töltse ki az összes mezőt</p>";
        else if ($_GET["error"] == "wronglogin") {
          echo "<p>Rossz bejelentkező adatok</p>";
        }
      }
      ?>
    </div>
  </div>
</body>
<script src="javas/login.js"></script>

</html>