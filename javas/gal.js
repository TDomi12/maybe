
const images = document.querySelectorAll('img');
const titles = document.querySelectorAll('a h3');

images.forEach((image, index) => {
  image.addEventListener('click', () => {
    const imageSrc = image.src;
    const imageAlt = image.alt;
    const title = titles[index].textContent;
    const purchasePage = 'purchase.php';
    window.location.href = `${purchasePage}?imageSrc=${imageSrc}&imageAlt=${imageAlt}&title=${title}`;
  });
});

