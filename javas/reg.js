
let toggleNavStatus = false;
let toggleNav = function(){
    let getSidebar = document.querySelector(".nav-sidebar");
    let getSidebarUL = document.querySelector(".nav-sidebar ul");
    let getSidebarTitle = document.querySelector(".nav-sidebar span");
    let getSidebarLinks = document.querySelectorAll(".nav-sidebar a");

    if(toggleNavStatus === false){
        getSidebarUL.style.visibility ="visible"; 
        getSidebar.style.width ="272px";
        getSidebarTitle.style.opacity ="1";

        let arrayLength = getSidebarLinks.length;
        for(let i = 0; i < arrayLength; i++) {
            getSidebarLinks[i].style.opacity = "1";

        }

        toggleNavStatus = true;

    }

    else if (toggleNavStatus === true){
        getSidebar.style.width ="0px";
        getSidebarTitle.style.opacity ="0";

        let arrayLength = getSidebarLinks.length;
        for(let i =0;i< arrayLength; i++){
            getSidebarLinks[i].style.opacity = "0";

        }
        getSidebarUL.style.visibility = "hidden";

        toggleNavStatus = false;

    }
}



let button = document.getElementById('color');

let buttonStatus = false;
button.addEventListener('click', () => {
    let sidebar = document.querySelector(".nav-sidebar");
    let mainbar = document.querySelector(".nav-main")

    if(buttonStatus === false){
        document.body.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";
        sidebar.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";
        mainbar.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";

        buttonStatus = true;

    }
    else if(buttonStatus === true){
        document.body.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        sidebar.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        mainbar.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        
        buttonStatus = false;
    }
});
