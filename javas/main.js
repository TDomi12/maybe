

let toggleNavStatus = false;
let toggleNav = function () {
    let getSidebar = document.querySelector(".nav-sidebar");
    let getSidebarUL = document.querySelector(".nav-sidebar ul");
    let getSidebarTitle = document.querySelector(".nav-sidebar span");
    let getSidebarLinks = document.querySelectorAll(".nav-sidebar a");

    if (toggleNavStatus === false) {
        getSidebarUL.style.visibility = "visible";
        getSidebar.style.width = "272px";
        getSidebarTitle.style.opacity = "1";

        let arrayLength = getSidebarLinks.length;
        for (let i = 0; i < arrayLength; i++) {
            getSidebarLinks[i].style.opacity = "1";

        }

        toggleNavStatus = true;

    }

    else if (toggleNavStatus === true) {
        getSidebar.style.width = "0px";
        getSidebarTitle.style.opacity = "0";
        getSidebarUL.style.visibility = "hidden";

        let arrayLength = getSidebarLinks.length;
        for (let i = 0; i < arrayLength; i++) {
            getSidebarLinks[i].style.opacity = "0";

        }


        toggleNavStatus = false;

    }
}



let button = document.getElementById('color');

let buttonStatus = false;
button.addEventListener('click', () => {
    let footer = document.querySelector("footer");
    let copy = document.querySelector(".copy p");
    let sidebar = document.querySelector(".nav-sidebar");
    let mainbar = document.querySelector(".nav-main")

    if (buttonStatus === false) {
        document.body.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";
        footer.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";
        copy.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";
        sidebar.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";
        mainbar.style.background = "linear-gradient(90deg, #bdc3c7, #2c3e50)";



        buttonStatus = true;
    }
    else if (buttonStatus === true) {
        document.body.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        footer.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        copy.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        sidebar.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";
        mainbar.style.background = "linear-gradient(90deg, #1f2224, #2d3940)";

        buttonStatus = false;
    }
});

const APIKEY = '04c35731a5ee918f014970082a0088b1';
const APIURL = ' https://api.themoviedb.org/3/discover/movie?sort_by=vote_average.desc&api_key=04c35731a5ee918f014970082a0088b1&page=1';
const SEARCHAPI = 'https://api.themoviedb.org/3/search/movie?&api_key=04c35731a5ee918f014970082a0088b1&query=';
const IMGW = 'https://image.tmdb.org/t/p/w500';

const main = document.getElementById('main');
const form = document.getElementById('form');
const search = document.getElementById('search');

getMovies(APIURL);

async function getMovies(url) {
    const resp = await fetch(url);
    const respData = await resp.json();

    console.log(respData);

    showMovies(respData.results);
}

function showMovies(movies) {
    main.innerHTML = '';
    movies.forEach(movie => {

        const { poster_path, title, vote_average, overview } = movie;
        const movieEl = document.createElement('div');
        movieEl.classList.add('movie');
        movieEl.innerHTML =
            `<img
      src="${IMGW + poster_path}"
      alt="${title}"
    />
    <div class="movie-info">
      <h3>${title}</h3>
      <span class="${getClassByRate(vote_average)}">${vote_average}</span>
    </div>

  <div class="overview">
  <h3>Overview:</h3>
  ${overview}</div>
  `;

        main.appendChild(movieEl);
    });

}

function getClassByRate(vote) {
    if (vote >= 8) {
        return "green";
    } else if (vote >= 5) {
        return "orange";
    } else {
        return "red";
    }
}

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const searchTerm = search.value;
    if (searchTerm) {

        getMovies(SEARCHAPI + searchTerm);

        search.value = '';
    }

});


let loader = document.querySelector('loader');

window.addEventListener('load', function () {
    loader.parentElement.removeChild(loader);
});

function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
}

